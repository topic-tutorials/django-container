# Django Continer

## Waht is the repository for ?

* Tutorial for integrating Django with Docker.


## Exercise

Uses "docker build" to build image of django enviroment.

### Step.1 downloads example code.

~~~
>> git clone https://gitlab.com/topic-tutorials/django-container.git && git checkout 0.0.1
>> git clone https://gitlab.com/topic-tutorials/django-hello-world.git && git checkout 0.0.1
~~~

### Step.2 uses "docker run" command to run Django service

~~~
>> cd docker-container

>> docker build . -t django-container

>> docker run -v /home/willis/Desktop/YilanUniversity/Class/Lesson1/Docker/django-hello-world/helloworld:/code/helloworld -p 8000:8000 --name django-hello-world  -it django-container python /code/helloworld/manage.py runserver 0.0.0.0:8000
~~~


### Step.3 uses "docker-compose" command to run Django service

~~~
>> cd docker-container

>> edit DJANGO_PROJECT_DIR of .env file

>> docker-compose up
~~~

### Step.4 Replace SQLite DB with PostgreSQL DB
~~~
>> git clone https://gitlab.com/topic-tutorials/django-container.git && git checkout 0.0.2
>> git clone https://gitlab.com/topic-tutorials/django-hello-world.git && git checkout 0.0.2
>> Edit django-container/.env
>> cd django-container/ && docker-compose up
~~~

### Step.5 Register docker hub and push customized django image to docker hub repository.
~~~

>> docker login

Username: willischen
Password:

WARNING! Your password will be stored unencrypted in /home/willis/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded

>> cd django-container && docker build . -t willischen/django
>> sudo docker push willischen/django
>> sudo docker rmi willischen/django
>> edit docker-compose.yml, replace "build: ." with "image: willischen/django".
>> sudo docker-compose up

~~~